package examenUF2;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class ProvesExprssions {

	@Test
	public void test2() {
		
		Primordial p = new Primordial();
		
		Assert.assertTrue(p.get_primordial(8)>200);
		Assert.assertFalse(p.get_primordial(6)>30);
		Assert.assertTrue(p.get_primordial(2)<=2);
		
	}

}
